export default class BaseService {

    _apiBase = 'https://localhost:4000';


    getResource = async (url) => {
        const res = await fetch(`${this._apiBase}${url}`);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();
    };

    addCompany = async () => {
        const res = await this.getResource(`/company/`);
        return res.results

    };

}