import React from 'react';
import {Route, Switch} from 'react-router-dom';
import { makeStyles} from '@material-ui/core/styles';
import Navbar from '../nav';
import Footer from '../Footer';
import RegisterCompany from "../RegisterCompany/RegisterCompany";
import RegisterHR from "../RegisterHR/RegisterHR";
import TabPanel from "../TabPanel";
import CardHR from "../CardHR";
import Vacancy from "../Vacancy";








const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.backgroundColor,
        position: 'relative',
        minHeight: '100vh'
    },
}));

export default function Page() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Navbar/>
            <Switch>
                <Route path='/company' component={RegisterCompany}/>
                <Route path='/hr' component={RegisterHR}/>
                <Route path='/hrs' component={TabPanel}/>
                <Route path='/card' component={CardHR}/>
                <Route path='/vacancy' component={Vacancy}/>

            </Switch>
            <Footer/>
        </div>
    )
}