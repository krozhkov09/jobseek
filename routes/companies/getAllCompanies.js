const Company = require("../../models/modelCompanies");

module.exports = (app) => {
    app.get("/companies", function (req, res) {
        if (req.query) {
            Company.find(req.query, (err, data) => {
                res.send({"Status": "Success", result: data});
            });
        } else {
            Company.find({}, (err, data) => {
                res.send({"Status": "Success", result: data});
            });
        }
    });
};
