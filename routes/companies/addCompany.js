const Company = require("../../models/modelCompanies");

module.exports = (app) => {
    app.post("/companies", function (req, res) {
        const company = new Company({
            companyName: req.body.companyName,
            phone: req.body.phone,
            email: req.body.email,
            country: req.body.country,
            city: req.body.city,
            companyWebsiteLink: req.body.companyWebsiteLink,
            facebookLink: req.body.facebookLink,
            linkedinLink: req.body.linkedinLink,
            companyDescription: req.body.companyDescription
        });
        company.save(function (err, data) {
            if (err) {
                res.send({status: "Error", message: err});
            } else {
                res.send(data);
            }
        });
    });
};
