const Company = require("../../models/modelCompanies");

module.exports = (app) => {
    app.put("/companies/:id", function (req, res) {
        const company = {};
        for (let key in req.body) {
            company[key] = req.body[key]
        }
        Company.updateOne({_id: req.params.id}, company, (err, result) => {
            if (err) {
                res.send({status: "Error", message:err});
            } else {
                res.send(req.body);
            }
        });
    });
};
