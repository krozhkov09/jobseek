const Company = require("../../models/modelCompanies");

module.exports = (app) => {
    app.get("/companies/:id", function (req, res) {
        Company.findById(req.params.id, (err, data) => {
            res.send({"Status": "Success", result: data});
        });
    });
};