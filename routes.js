const updateCompany = require("./routes/companies/updateCompany");
const delCompany = require("./routes/companies/delCompany");
const addCompany = require("./routes/companies/addCompany");
const getAllCompany = require("./routes/companies/getAllCompanies");
const getOneCompany = require("./routes/companies/getOneCompany");


module.exports = (app) => {
    updateCompany(app);
    delCompany(app);
    addCompany(app);
    getAllCompany(app);
    getOneCompany(app)
};