const mongoose = require("mongoose");

const Company = mongoose.model("company", {
    companyName: {
        type: String,
        required: true},
    phone: {
        type:String,
        required: true,
         min: 9,
         max: 11},
    email: {
        type:String,
        required: true},
    country: {
        type:String,
        required: true},
    city: {
        type: String,
        required: true},
    companyWebsiteLink: {
        type: String,
        required: true},
    facebookLink: {
        type: String,
        required: true},
    linkedinLink: {
        type: String,
        required: true},
    companyDescription: {
        type: String,
        required: true}
});
module.exports = Company;